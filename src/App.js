import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Icon } from "react-icons-kit";
import { x } from "react-icons-kit/feather/x";
import { androidMenu } from "react-icons-kit/ionicons/androidMenu";
import Button from "./components/Button";
import { Container } from "./components/Container";
import MainContentWrapper, {
  LogoImageContainer,
  SidebarButton,
  MainContentSection,
  NormalClockWrapper,
  MainWrapper,
  SideBar,
  Overlay,
  FooterSection,
  SidebarContent,
  SidebarClose,
} from "./style/index.style";
import { WalletConnector } from "./components/WalletConnector";
import DataContainer from "./components/DataContainer/DataContainer";
import { SidebarContainer } from "./components/SidebarContainer/SidebarContainer";
import "react-toastify/dist/ReactToastify.css";
import Footer from "./components/footer";

const IndexPage = (props) => {
  const [state, setState] = useState({
    toggle: false,
  });
  const [connected, setConnected] = useState(false);
  const [address, setAddress] = useState(undefined);
  const [wallet, setWallet] = useState(undefined);
  const [farmingRewards, setFarmingRewards] = useState(undefined);
  const [currentUsdValue, setCurrentUsdValue] = useState(null);
  const [showCurrentValue, setShowCurrentValue] = useState(false);
  const [isFetched, setIsFetched] = useState(false);

  const toggleHandle = () => {
    setState({
      ...state,
      toggle: !state.toggle,
    });
  };

  return (
    <React.Fragment>
      <MainWrapper className={state.toggle === true ? "sidebar-open" : ""}>
        <MainContentWrapper>
          <LogoImageContainer>
            <SidebarButton>
              {state.toggle === false && (
                <Button
                  onClick={toggleHandle}
                  icon={<Icon icon={androidMenu} size={25} />}
                ></Button>
              )}
            </SidebarButton>
          </LogoImageContainer>
          <Container className="mainContainer">
            <MainContentSection>
              <h2 style={{ fontWeight: "bold" }}>Rewards</h2>
              <p>Explore all the rewards you've got so far!</p>
              <NormalClockWrapper>
                <DataContainer
                  connected={connected}
                  address={address}
                  farmingRewards={farmingRewards}
                  setFarmingRewards={setFarmingRewards}
                  currentUsdValue={currentUsdValue}
                  setCurrentUsdValue={setCurrentUsdValue}
                  setShowCurrentValue={setShowCurrentValue}
                  setIsFetched={setIsFetched}
                />
              </NormalClockWrapper>
              <WalletConnector
                connected={connected}
                setConnected={setConnected}
                address={address}
                setAddress={setAddress}
                wallet={wallet}
                setWallet={setWallet}
                toggleHandle={toggleHandle}
                setFarmingRewards={setFarmingRewards}
                setCurrentUsdValue={setCurrentUsdValue}
                farmingRewards={farmingRewards}
              />
            </MainContentSection>
            <Footer />
          </Container>
        </MainContentWrapper>
        <SideBar className={state.toggle === true ? "expand" : ""}>
          <SidebarContent>
            <SidebarClose type="submit" aria-label="close">
              <Icon icon={x} size={33} onClick={toggleHandle} />
            </SidebarClose>
            <SidebarContainer
              showCurrentValue={showCurrentValue}
              setShowCurrentValue={setShowCurrentValue}
              currentUsdValue={currentUsdValue}
              farmingRewards={farmingRewards}
              setCurrentUsdValue={setCurrentUsdValue}
              currentUsdValue={currentUsdValue}
              setFarmingRewards={setFarmingRewards}
              isFetched={isFetched}
            />
          </SidebarContent>
          <Overlay className={state.toggle === true ? "expand" : ""} />
        </SideBar>
      </MainWrapper>
    </React.Fragment>
  );
};

export default IndexPage;
