import React from "react";
import { Row, Col } from "reactstrap";
import styled from "styled-components";

const A = styled.a`
  color: rgb(47, 128, 237);
`;

const StyledFooter = styled.footer`
  padding-top: 140px;
  margin-bottom: 30px;
  .footer-copyright {
    @media only screen and (max-width: 600px) {
      margin-bottom: 30px;
    }
  }
  @media only screen and (max-width: 600px) {
    margin-top: 50px !important;
    padding-top: 60px !important;
  }
`;
const Footer = () => {
  function renderFooterLink() {
    return (
      <A href="https://apy.vision/#/membership" target="_blank">
        track your liquidity pool gains
      </A>
    );
  }

  return (
    <StyledFooter className="footer">
      <Row style={{ justifyContent: "center" }}>
        <Col
          md="6"
          className="footer-copyright"
          style={{
            display: "flex",
            justifyContent: "center",
            textAlign: "center",
          }}
        >
          <p className="mb-0" style={{ color: "white" }}>
            © Built by APY.Vision - {renderFooterLink()}
          </p>
        </Col>
      </Row>
    </StyledFooter>
  );
};
export default Footer;
