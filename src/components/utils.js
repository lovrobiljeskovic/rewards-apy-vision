export const toDecimalPlaces = input => {
  const num = Math.abs(input)
  if (num === 0) {
    return 0.0
  }
  const digits = toDecimalPlaceDigits(input)
  return input?.toFixed(digits)
}

export const toDecimalPlaceDigits = input => {
  const num = Math.abs(input)
  if (num === 0) {
    return 2
  }
  if (num > 1.1) {
    return 2
  }
  if (num > 0.1) {
    return 3
  }
  if (num > 0.01) {
    return 4
  }
  if (num > 0.001) {
    return 5
  }
  if (num > 0.0001) {
    return 6
  }
  return 13
}

export const truncAddrs = addrs => {
  return `${addrs.substring(0, 5)}..${addrs.substring(addrs.length - 3)}`
}

