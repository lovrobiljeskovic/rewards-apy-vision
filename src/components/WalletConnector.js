import React, { useState, useCallback, useEffect, useMemo } from "react";
import Onboard from "bnc-onboard";
import Web3 from "web3";
import { Button } from "reactstrap";
import styled from "styled-components";
import { toast, ToastContainer } from "react-toastify";
import { truncAddrs } from "./utils";

const Container = styled.div`
  padding-top: 40px;
  display: flex;
  justify-content: center;
`;

const customId = "custom-id-yes";

export const WalletConnector = (props) => {
  const onboard = useMemo(() => {
    return Onboard({
      networkId: 1,
      subscriptions: {
        wallet: (wallet) => {
          if (wallet.provider) {
            props.setWallet(wallet);
            const web3 = new Web3(wallet.provider);
            if (wallet.name) {
              localStorage.setItem("walletName", wallet.name);
            } else {
              localStorage.removeItem("walletName");
            }
          } else {
            props.setConnected(false);
          }
        },
        address: (account) => {
          if (account === undefined) {
            localStorage.removeItem("walletName");
            props.setConnected(false);
            onboard.walletReset();
          }
          props.setAddress(account);
        },
      },
    });
  }, []);

  const connect = useCallback(
    async (walletName) => {
      await onboard.walletSelect(walletName);

      let checkPassed = false;
      try {
        checkPassed = await onboard.walletCheck();
      } catch (error) {
        console.error(error);
      }
      if (checkPassed) {
        props.setConnected(true);
      } else {
        localStorage.removeItem("walletName");
        onboard.walletReset();
        props.setConnected(false);
        props.setWallet(undefined);
      }
    },
    [onboard, props]
  );

  const hasFarmingRewards =
    props.farmingRewards && Object.keys(props.farmingRewards).length > 0;

  useEffect(() => {
    {
      props.connected && hasFarmingRewards && props.toggleHandle();
    }
  }, [props.connected, hasFarmingRewards]);

  useEffect(() => {
    {
      props.connected &&
        toast.success(`Connected as ${truncAddrs(props.address)}`, {
          position: "top-center",
          autoClose: 3000,
          toastId: customId,
          style: {
            background: "rgb(47,128,237)",
          },
        });
    }
  }, [props.connected]);

  useEffect(() => {
    const previouslySelectedWallet = localStorage.getItem("walletName");

    if (previouslySelectedWallet && onboard.walletSelect) {
      connect(previouslySelectedWallet).catch((error) => {
        console.error(error);
      });
    }
  }, [onboard, connect]);

  const reset = useCallback(() => {
    onboard.walletReset();
    localStorage.removeItem("walletName");
    props.setWallet(undefined);
    props.setConnected(false);
    props.setFarmingRewards(undefined);
    props.setCurrentUsdValue(null);
    props.toggleHandle();
  }, [onboard, props]);
  return (
    <Container>
      <Button
        style={{
          backgroundImage:
            "linear-gradient( -31deg,rgb(86,204,242) 0%,rgb(47,128,237) 100% )",
          borderRadius: 28,
          width: 200,
          height: 48,
          border: "none",
        }}
        onClick={props.connected ? reset : connect}
        type="submit"
      >
        {props.connected ? "Disconnect" : "Connect"}
      </Button>
      <ToastContainer />
    </Container>
  );
};
