import React, { useState, useEffect } from "react";
import "./DataContainer.css";
import axios from "axios";
import { API_URL } from "../constants";
import { toDecimalPlaces } from "../utils";
import CountUp from "react-countup";
import { toast } from "react-toastify";
import ClipLoader from "react-spinners/ClipLoader";

const NormalUnitContainer = ({ value, label, prefix }) => {
  return (
    <div className={"NormalUnitContainer"}>
      <CountUp
        prefix={prefix}
        className="NormalupperCard"
        start={0}
        end={parseInt(value)}
      />
      <div className="digitLabel">
        <label style={{ fontSize: 10 }}>{label}</label>
      </div>
    </div>
  );
};

export const DataContainer = (props) => {
  let [loading, setLoading] = useState(true);
  useEffect(() => {
    const fetchData = async () => {
      if (props.connected && props.address) {
        const response = await axios.get(
          `${API_URL}/farming/rewardsByUser/${props.address}`
        );
        if (response.data) {
          props.setFarmingRewards(response.data);
          props.setIsFetched(true);
        } else {
          toast.error(
            "Whoops - we didn't find any claimed reward history for this account. Please check back later!",
            {
              position: toast.POSITION.TOP_CENTER,
              autoClose: 3000,
            }
          );
        }
        setLoading(!loading);
      }
    };

    fetchData();
  }, [props.connected, props.address]);

  const numberOfTxs =
    props.farmingRewards &&
    Object.values(props.farmingRewards)
      .map((farmingRewardEntry) => {
        return farmingRewardEntry.transfers.length;
      })
      .reduce((a, b) => a + b, 0);

  const farmingRewardsAggregated =
    props.farmingRewards &&
    Object.entries(props.farmingRewards)
      .map((farmingRewardEntry) => {
        return farmingRewardEntry[1].transfers
          .map((transfer) => {
            return transfer.amount * transfer.usdPrice;
          })
          .reduce((a, b) => a + b, 0);
      })
      .reduce((a, b) => a + b, 0);

  return (
    <div>
      {farmingRewardsAggregated > 0 && numberOfTxs > 0 ? (
        <div className={"NormalClock"}>
          <NormalUnitContainer
            prefix="#"
            value={numberOfTxs ? numberOfTxs : 0}
            label="Number of transactions"
          />
          <NormalUnitContainer
            prefix="$"
            value={
              farmingRewardsAggregated
                ? toDecimalPlaces(farmingRewardsAggregated)
                : 0
            }
            label="Total farming collected"
          />
        </div>
      ) : loading ? (
        <ClipLoader loading={loading} color="rgb(47, 128, 237)" size={150} />
      ) : (
        <div className={"NormalClock"}>
          <NormalUnitContainer
            prefix="#"
            value={0}
            label="Number of transactions"
          />
          <NormalUnitContainer
            prefix="$"
            value={0}
            label="Total farming collected"
          />
        </div>
      )}
    </div>
  );
};

export default DataContainer;
