import React, { useEffect } from "react";
import styled from "styled-components";
import { Card, CardBody } from "reactstrap";
import { toDecimalPlaces } from "../utils";
import axios from "axios";
import { API_URL } from "../constants";

const CardContainer = styled.div`
  border-radius: 40px;
  .tag-hover-effect {
    opacity: 0;
    transition: 1.5s;
  }
  &:hover {
    .tag-hover-effect {
      opacity: 1;
    }
  }
`;

const DotsGroup = styled.span`
  .dots1 {
    width: 8px;
    height: 8px;
    bottom: 0;
    right: 6%;
  }
  .dots2 {
    right: 15%;
    bottom: -1%;
  }
  .dots3 {
    right: 1%;
    bottom: 9%;
  }
  .dots4 {
    right: 5%;
    bottom: 20%;
  }
  .dots5 {
    right: 7%;
    bottom: 10%;
  }
  .dots6 {
    right: 9%;
    bottom: 29%;
  }
  .dots7 {
    right: -1%;
    bottom: 22%;
  }
  .dots8 {
    right: 14%;
    bottom: 12%;
  }
  .dots9 {
    right: 20%;
    bottom: 34%;
  }
  .dot-small {
    width: 4px;
    height: 4px;
  }
  .dot-small-semi {
    width: 5px;
    height: 5px;
  }
  .dot-medium {
    width: 6px;
    height: 6px;
  }
  .dots {
    position: absolute;
    background-color: black;
    border-radius: 100%;
  }
`;

export const SidebarContainer = (props) => {
  const fetch = async (transfers, pool, current) => {
    let response = null;
    response = await axios.get(
      `${API_URL}/price/${transfers[0].tokenAddress}/`
    );
    const calculatedValue =
      Object.values(transfers).reduce(
        (a, b) => a + (b.amount * response.data || 0),
        0
      ) || 0;
    props.setFarmingRewards({
      ...props.farmingRewards,
      [pool]: {
        ...props.farmingRewards[pool],
        usdPrice: calculatedValue,
        current: current === "valueAtBlock" ? "currentPrice" : "valueAtBlock",
      },
    });
  };

  const renderCollectedRewards = (amount, symbol) => {
    if (amount && symbol) {
      return (
        <>
          <h5 className="mb-3">
            {toDecimalPlaces(amount)} {symbol}
          </h5>
        </>
      );
    }

    return <h5 className="mb-3">-</h5>;
  };

  const getTotalUsdValueAtBlock = (transfers, pool) => {
    if (transfers) {
      const calculatedValue =
        Object.values(transfers).reduce(
          (a, b) => a + (b.amount * b.usdPrice || 0),
          0
        ) || 0;
      return calculatedValue;
    }
  };

  useEffect(() => {
    if (props.farmingRewards) {
      const newFarmingRewards = {};
      Object.entries(props.farmingRewards).forEach((entry) => {
        const transfers = entry[1].transfers;
        const pool = entry[0];
        newFarmingRewards[pool] = {
          ...props.farmingRewards[pool],
          valueAtBlock: getTotalUsdValueAtBlock(transfers, pool),
        };
      });
      props.setFarmingRewards(newFarmingRewards);
    }
  }, [props.isFetched]);

  const changeCurrent = (pool, current) => {
    props.setFarmingRewards({
      ...props.farmingRewards,
      [pool]: {
        ...props.farmingRewards[pool],
        current: current === "valueAtBlock" ? "currentPrice" : "valueAtBlock",
      },
    });
  };

  return (
    <div>
      <CardContainer>
        <h1
          style={{
            textAlign: "center",
            marginBottom: 30,
            color: "white",
            fontFamily: "Anton,sans-serif",
            lineHeight: 1.35,
          }}
        >
          Farming Overview
        </h1>
        {props.farmingRewards ? (
          Object.entries(props.farmingRewards).map((entry) => {
            const rewards = entry[1].rewards;
            const transfers = entry[1].transfers;
            const pool = entry[0];
            return (
              <Card key={pool} style={{ borderRadius: 40, marginBottom: 20 }}>
                <CardBody>
                  <div>
                    <h6 style={{ fontFamily: "Lato" }}>Collected Rewards:</h6>{" "}
                    {renderCollectedRewards(
                      rewards[0].amount,
                      rewards[0].symbol
                    )}
                    <h6 style={{ fontFamily: "Lato" }}>
                      Total USD Value&nbsp;
                      {entry[1].current === "valueAtBlock" ? (
                        <>
                          @ Time Of Reward /&nbsp;
                          <a
                            style={{ fontFamily: "Lato" }}
                            onClick={() =>
                              fetch(transfers, pool, entry[1].current)
                            }
                            className="anchor-link underline"
                          >
                            @ Current Price
                          </a>
                        </>
                      ) : (
                        <>
                          <a
                            style={{ fontFamily: "Lato" }}
                            onClick={() =>
                              changeCurrent(pool, entry[1].current)
                            }
                            className="anchor-link underline"
                          >
                            @ Time Of Reward
                          </a>
                          &nbsp;/ @ Current Price
                        </>
                      )}
                      :
                    </h6>{" "}
                    <h5 style={{ fontFamily: "Lato" }} className="mb-3">
                      $
                      {entry[1].current === "currentPrice" &&
                      props.farmingRewards
                        ? toDecimalPlaces(
                            props.farmingRewards[entry[0]].usdPrice
                          )
                        : toDecimalPlaces(
                            props.farmingRewards[entry[0]].valueAtBlock
                          )}{" "}
                      USD
                    </h5>
                  </div>
                  <span className="tag-hover-effect">
                    <DotsGroup>
                      <span className="dots dots1"></span>
                      <span className="dots dots2 dot-small"></span>
                      <span className="dots dots3 dot-small"></span>
                      <span className="dots dots4 dot-medium"></span>
                      <span className="dots dots5 dot-small"></span>
                      <span className="dots dots6 dot-small"></span>
                      <span className="dots dots7 dot-small-semi"></span>
                      <span className="dots dots8 dot-small-semi"></span>
                      <span className="dots dots9 dot-small"> </span>
                    </DotsGroup>
                  </span>
                </CardBody>
              </Card>
            );
          })
        ) : (
          <div></div>
        )}
      </CardContainer>
    </div>
  );
};
